<?php


namespace core;


class DBQuery
{
    protected $type;
    protected $filelds;
    protected $where;
    protected $tableName;
    protected  $insertingRow;
    protected $updatingrow;
    protected $isOneRow;
    /**
    *тут задаємо в конструкторі назву таблиці
     *
     */
    public function __construct($tableName)
    {
        $this->tableName=$tableName;
        $this->type=null;
        $this->filelds='*';
        $this->where=[];
        $this->insertingRow=null;
        $this->isOneRow=false;
    }

    public function insert($row){
        $this->type='INSERT';
        $this->insertingRow=$row;
        return $this;
    }

    /**
    *дана функція буде означати за вибір значень
     */
    public function select($fields ='*'){
        $this->type='SELECT';
        $this->filelds=$fields;
        return $this;
    }

    /**
     * @return null
     */
    public function update($row)
    {
      $this->type='UPDATE';
      $this->updatingrow=$row;
      return $this;
    }



    /**
     * @return type delete
     */
    public function delete()
    {
        $this->type='DELETE';
return $this;
    }


    public function where($condition){
        if(is_string($condition)){
         array_push($this->where, $condition);
        }
        if(is_array($condition)){
           $this->where=array_merge($this->where,$condition);
        }
return $this;
    }



    protected function generateWherePart($where){

            $fieldsList = array_keys($where);
            $valuesList=array_values($where);
            $whereComponents=[];
            foreach ($fieldsList as $item){
                array_push($whereComponents,"{$item} = :{$item}");
            }
            $wherePart=implode(' AND ', $whereComponents);
           return $wherePart;

    }
    public function one(){
        $this->isOneRow=true;
  return $this;
    }
    public function isOne(){
        return $this->isOneRow;
    }

    /**
     * @return
     */
    protected function generateParamsArray($row)
    {
        $params=[];
        foreach ($row as $key=>$item){
            $params[':'.$key]=$item;
        }
        return $params;
    }

    public function getSql(){
       switch ($this->type){
           case 'UPDATE':
                $wherePart=$this->generateWherePart($this->where);
                $setPartsArray=[];
                $params=[];
                foreach ($this->updatingrow as $key=>$item){
                    array_push($setPartsArray,$key.' = :'.$key);
                    $params[':'.$key]=$item;
                }
               foreach ($this->where as $key=>$item){
                    $params[':'.$key]=$item;
               }

                $setPartsString=implode(',',$setPartsArray);
                $sql="UPDATE {$this->tableName} SET {$setPartsString} WHERE  {$wherePart}";
               return ['sql'=>$sql,'params'=>$params];

               break;
           case 'DELETE':
               $wherePart=$this->generateWherePart($this->where);
              $sql="DELETE FROM {$this->tableName} WHERE {$wherePart}";
              $params=$this->generateParamsArray($this->where );

               return ['sql'=>$sql,'params'=>$params];
               break;
           case 'SELECT':
               if(is_string($this->filelds))
                   $fieldPart=$this->filelds;
               else
                   if(is_array($this->filelds)){
                    $fieldPart=implode(', ',$this->filelds);
                   }
                   else return null;
               $sql="SELECT {$fieldPart} FROM {$this->tableName}";
               if(!empty($this->where)){
//                   $fieldsList = array_keys($this->where);
//                  $valuesList=array_values($this->where);
//                   $whereComponents=[];
//                   foreach ($fieldsList as $item){
//                       array_push($whereComponents,"{$item} = :{$item}");
//                   }

                   $wherePart=$this->generateWherePart($this->where);
                   $sql=$sql." WHERE {$wherePart}";
               }
               $params=$this->generateParamsArray($this->where );
               return ['sql'=>$sql,'params'=>$params];
               break;
           case 'INSERT':
                $fileldList=array_keys($this->insertingRow);
                $valuesList=array_values($this->insertingRow);
                $fieldsLlistSrting=implode(', ',$fileldList);
                $valuesParamsList=[];
               $params=[];
               foreach ($this->insertingRow as $key=>$item){
                   array_push($valuesParamsList,":{$key}");
                   $params[':'.$key]=$item;
               }
               $valuesListString=implode(', ',$valuesParamsList);
                   $sql="INSERT INTO {$this->tableName} ({$fieldsLlistSrting})VALUES  ($valuesListString)";
               return ['sql'=>$sql,'params'=>$params];
               break;
       }
//       echo $this->type;
//       echo $this->filelds;
       return  null;
    }


    public function getSql2($e){



    }



}
































