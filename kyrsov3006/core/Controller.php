<?php


namespace core;

/**
*Базовий клас для всіх контролерів
 *@package  core
 */
class Controller
{
    public function render($vieaName,$localParams=null,$globalParans=null)
    {
$tpl=new Template();
if(is_array($localParams))
$tpl->setParams($localParams);
if(!is_array($globalParans))
    $globalParans=[];
$moduleName=strtolower((new \ReflectionClass($this))->getShortName());
$globalParans['Content']=$tpl->render("./views/{$moduleName}/{$vieaName}.php");
return $globalParans;
    }
    public function isGet(){
return $_SERVER['REQUEST_METHOD']=='GET';
    }
    public function isPost(){
return $_SERVER['REQUEST_METHOD']=='POST';
    }

    /***
     * Фільтрація асоціативного массиву
     * @param $array array Асоціативний масив
     * @param $keys array Массив ключів, які потібно залишити
     * @return array Відфільтрований асоціативний масив
     */
    public function formFilter($array,$keys){
       /* var_dump($array);
        var_dump($keys);*/
        $res=[];
        foreach ($array as $kay=>$value){
            if(in_array($kay,$keys)){
                $res[$kay]=$value;
            }
        }
        return $res;
    }

    /***
     * Фільтрація масиву Post- змінних
     * @param $keys ключі, які потрібно залишити
     * @return array Відфільтрований асоціативний масив
     *
     */
    public function postFileter($keys){
        return $this->formFilter($_POST,$keys);
    }
}