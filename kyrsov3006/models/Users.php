<?php

namespace models;

class Users extends \core\Model
{

    public function Authenticate($login, $password)
    {
        global $core;
        $query = new \core\DBQuery('users');

        $res = $core->getDB()->executeQuery($query->select('COUNT(*) as count')->where(['login' => $login, 'password' => $password])->one());
        $_SESSION['name'] = $login;

        session_start();
        $_SESSION['count'] = $res['count'];
       if($res['count'] != 0){
           $q= $core->getDB()->executeQuery($query->select()->where(['login' => $login]));
          $_SESSION['who']=$q['id'];
           $_SESSION['login'] = $q['login'];
           $_SESSION['password'] = $q['password'];
           $_SESSION['lastname'] = $q['lastname'];//імя
           $_SESSION['firstname'] = $q['firstname'];//адрес доставкі
           return $res['count'] != 0;
       }else {
           $_SESSION['regis_count'] = 'Такого користувача не існує';
       }

        /*
        var_dump($query->select('COUNT(*)')->where(['login'=>$login,'password'=>$password]));
        var_dump($res);*/
    }

    public function Registrate($login, $password, $name, $adress)
    {
        session_start();

        global $core;
        $query = new \core\DBQuery('users');

        $res = $core->getDB()->executeQuery($query->select('COUNT(*) as count')->where(['login' => $login])->one());
        if ($res['count'] == 0) {

            $core->getDB()->executeQuery($query->insert(['login' => "$login", 'password' => "$password", 'firstname' => "$name", 'lastname' => "$adress"]));


         //   $_SESSION['regis_count'] = "Все вийшло";
            $_SESSION['regis_count']= " Такий користувач почав існувати";

        } else {
            $_SESSION['regis_count']=" Такий користувач вже існує";
           // $_SESSION['regis_count'] = "не все вийшло";
        }

        // return $res['count'];

    }

    //пробую сортувати товар
    public function ShowTovar($show=1, $sort = 'auto')
    {
        global $core;
        $sorting = '';
        $qwe = '';
        switch ($sort) {
            case'price-asc':
                $qwe = '`price` ASC';
                break;
            case'price-desc':
                $qwe = '`price` DESC';
                break;
            case'popular':
                $qwe = '`count` DESC';
                break;
            case'news':
                $qwe = '`datatime` DESC';
                break;
            case'brand':
                $qwe = '`brand`';
                break;
            default:
                $qwe = 1;
                break;
        }


        switch ($show) {
            case'laptop':
                $sorting = 'laptop';
                break;
            case'mobile':
                $sorting = 'mobile';
                break;
            case'tablet':
                $sorting = 'tablet';
                break;

            default:
                $sorting = 1;
                break;
        }
        $query = new \core\DBQuery('table_products');
        $d['count'] = '';
       if ($qwe == 1) {
            if ($sorting == 1) {
                $d['count'] = $core->getDB()->executeQuery($query->select()->where(['products_id DESC']));

            } else {
                $d['count'] = $core->getDB()->executeQuery($query->select()->where(['type_tovara' => "$sorting"]));

            }
        }
        else{

//echo $qwe .'  -  '.$sorting;

            $d['count'] = $core->getDB()->executeQuery2($qwe,$sorting);

        }

        return $d['count'];

//        $query->select()->where(['login'=>'user','password'=>'user']);
    }

//змінюємо параметри користувача

    public function RestarAcaunt($id,$email,$password,$name,$adress)
    {  global $core;
        $query = new \core\DBQuery('users');
        //$this->Model->update(['login'=> $email,'password'=>$password,'lastname'=>$name,'firsname'=>$adress])->where(['id'=>$id]);
       // ['login'=> $email,'password'=>$password,'lastname'=>$name,'firsname'=>$adress]

        $core->getDB()->executeQuery($query->update(['login'=> $email,'password'=>$password,'lastname'=>$name,'firstname'=>$adress])->where(['id'=>$id]));
    }

    //тут я перекидаю ціну товару
    public function ShowPrice()
    {
        global $core;
        $query = new \core\DBQuery('table_products');
        $d['count'] = '';
        $d['count'] = $core->getDB()->executeQuery($query->select()->where(['products_id DESC']));
        $qwe[] = '';
        foreach ($d['count'] as $key => $value) {
            $qwe[$value['id']] = $value['price'];

            // echo  '<br>'.$value['id'].'  -  '.$value['price'];
        }
        /* var_dump($qwe);

         var_dump($qwe);*/
        unset($qwe[0]);


        return $qwe;
    }

//коли користувач виходить з свого кабінету
    public function somExit()
    {
        session_start();
        $_SESSION['login'] = '';
        $_SESSION['password'] = '';
        $_SESSION['lastname'] = '';
        $_SESSION['firstname'] = '';
        $_SESSION['who']='';
        $_SESSION['regis_count']='';
        $_SESSION['name'] = '';
       // $_SESSION['eror']='';
        $_SESSION['count'] = 0;
        $_SESSION['regis_count2']='';
    }


}