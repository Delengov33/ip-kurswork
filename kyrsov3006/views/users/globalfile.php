<div class="">


    <div class="container mt-4 ">
        <div class="row">
            <div class=" col-6">

                <a class='text-center' href='/users/product?id=16' data-id='16'>
                    <img class="d-block  w-100 " src="/views/layout/img/some_slider/b6.jpg" alt="Second">
                </a>
            </div>
            <div class=" col-6">
                <a class='text-center' href='/users/product?id=6' data-id='6'>
                    <img class="d-block  w-100 " src="/views/layout/img/some_slider/b4.jpg" alt="Second">
                </a>
            </div>
        </div>
        <div class="row mt-1">
            <div class=" col-6 ">
                <a class='text-center' href='/users/product?id=9' data-id='9'>
                    <img class="d-block  w-100 " src="/views/layout/img/some_slider/b7.jpg" alt="Second">
                </a>
            </div>
            <div class=" col-6">
                <a class='text-center' href='/users/product?id=32' data-id='32'>
                    <img class="d-block  w-100  " src="/views/layout/img/some_slider/b5.jpg" alt="Second">
                </a>
            </div>
        </div>
    </div>

    <div class=" mt-3 row">
        <div class=" col-12 h4 bg-light text-dark p-1 rounded mybtn" >
            Ласкаво просимо в інтернет - магазин електроніки Delen!
        </div>
        <div class=" col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10 h6 text-dark text-justify">


            Нашою основною перевагою є кращий асортимент, зручність покупки онлайн, відмінні ціни і гнучкі можливості доставки.
            На нашому сайті ви зможете вибрати і купити смартфони найпопулярніших брендів,
            підібрати найбільш підходящий для себе планшет, познайомитися і придбати сучасні моделі  ноутбуків за розумні гроші.
            І це тільки мала частина товарів, які ви зможете знайти в один клік в нашому інтернет - магазині Алло.
            Якщо у Вас виникли будь - які питання - будь ласка, телефонуйте нам за номером 38066 674 8726
            і наші фахівці з радістю дадуть відповідь на ваші запитання.
        </div>
    </div>





</div>
