<?php
session_start();
/**
 * @var $Title string
 * @var $Content string
 */
$auroficate = $_SESSION['count'];

$name=$_SESSION['name'];
$eror=$_SESSION['regis_count'];
//echo $auroficate;
?>



<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<!--    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"> </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="http://kyrsov3006/views/layout/css/somestyle.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!--    <title>--><?//= $Title ?><!--</title>-->

</head>
<body>

<header class="page-header">
    <div class="page-header_navbar">
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/users/globalfile">Delen</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/users/globalfile">Головна<span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Каталог товарів
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <h6>Laptop</h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/users/all?show=laptop&sort=auto">Ноутбукі</a>
                                <h6>Smartphones</h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/users/all?show=mobile&sort=auto">Телефони</a>
                                <h6>Tablets</h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/users/all?show=tablet&sort=auto">Планшети</a>
                                <h6>All</h6>

                                <a class="dropdown-item" href="/users/all?show=all&sort=auto">Всі</a>


                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/users/aboutus">Про нас</a>
                        </li>

                    </ul>
                    <!--                    <form class="form-inline my-2 my-lg-0" method="post" action="/users/login">-->
                    <!--                        <input class="form-control mr-sm-2" name="login" type="email" placeholder="Логін" >-->
                    <!--                        <input class="form-control mr-sm-2" type="password" name="password" placeholder="Пароль" >-->
                    <!--                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Вхід</button>-->
                    <!--                    </form>-->


                    <?
                    if ($_SESSION['count'] == 0) {
                        ?>
                        <button type="button" class="btn btn-outline-success my-2 my-sm-0" data-toggle="modal"
                                data-target="#exampleModalCenter">
                            <!--                        <img src="/img/user.png" alt="users">-->
                            <!--                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>-->

                            <i class="fa fa-user"></i>
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Не чекайте, увійдіть
                                            зараз!</h5>


                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <ul class="nav nav-tabs">
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link active" data-toggle="tab"
                                                   href="#description">Ввійти</a>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link" data-toggle="tab" href="#characteristics">Зареєструватися</a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="description">

                                                <form class="form my-2 my-lg-0" method="post" action="/users/login">
                                                    <div class="form-group">
                                                        <label for="InputEmail1">Логін</label>
                                                        <input class="form-control mr-sm-2" name="login"
                                                               id="InputEmail1" type="email" placeholder="Логін">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Password1">Пароль</label>
                                                        <input class="form-control mr-sm-2" type="password"
                                                               id="Password1" name="password" placeholder="Пароль">
                                                    </div>
                                                    <button class="btn btn-primary my-2 my-sm-0" type="submit">Вхід
                                                    </button>
                                                </form>

                                            </div>
                                            <div class="tab-pane" id="characteristics">

                                                <form class="form my-2 my-lg-0" method="post" action="/users/register">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Поштова адреса</label>
                                                        <input type="email" class="form-control" id="exampleInputEmail1"
                                                               aria-describedby="emailHelp" placeholder="email" name="login">

                                                    </div>
                                                    <div class="from-group">
                                                        <label for="name1">Імя</label>
                                                        <input type="text" class="form-control"
                                                               id="name1" placeholder="імя" name="name">
                                                    </div>
                                                    <div class="from-group">
                                                        <label for="adress">Адресу доставки</label>
                                                        <input type="text" class="form-control"
                                                               id="adress" placeholder="Адресса" name="adress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Пароль</label>
                                                        <input type="password" class="form-control"
                                                               id="exampleInputPassword1" placeholder="Пароль" name="password1">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword2">Повторіть пароль</label>
                                                        <input type="password" class="form-control"
                                                               id="exampleInputPassword2"
                                                               placeholder="Повторіть пароль" name="password2">
                                                    </div>

                                                    <button type="submit" class="btn btn-primary">Зареєструватися</button>

                                                </form>
                                            </div>

                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <div class="badge bg-danger p-2" >
                                           <?=$eror?>
                                        </div>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>


                    <?
                    } else {
                        ?>

                        <button type="button" class="btn btn-outline-info mr-2 my-sm-0" data-toggle="modal">
                            <!--                        $_SESSION['count']=0;-->


                            <a class="fa fa-user" href="/users/profile">Профіль</a>
                        </button>
                        <button type="button" class="btn btn-outline-danger my-2 my-sm-0" data-toggle="modal">
                            <!--                        $_SESSION['count']=0;-->


                            <a class="fa fa-sign-out" href="/users/exit"> <?=$name?></a>
                        </button>

                        <button type="button" class="btn btn-outline-info my-2 my-sm-0 ml-1" data-toggle="modal">
                            <!--                        $_SESSION['count']=0;-->


                            <a class="fa fa-shopping-basket" href="/users/basket">Корзина</a>
                            <h7 class="text-danger  mini-cart" id="mini-cart">0</h7>
                        </button>


                    <? } ?>

                </div>
            </nav>
        </div>
    </div>

    <div class="page-header_carosel">
        <div class="container">
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100 " src="/views/layout/img/some_slider/b1.jpg" alt="First">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block  w-100" src="/views/layout/img/some_slider/b2.jpg" alt="Second">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/views/layout/img/some_slider/b3.jpg" alt="Third">
                    </div>
                </div>
            </div>


        </div>
    </div>


</header>



<div class="container content123">

    <?= $Content ?>

</div>



<footer class="section footer-classic context-dark bg-image mt-5 pt-3" class="footer1" style="background: #2d3246;">

        <div class="container">
            <div class="row text-center text-xs-center text-sm-left text-md-left text-muted">
                <div class="container">
                    <div class="row row-30">
                        <div class="col-md-4 col-xl-5">
                            <div class="pr-xl-4"><a class="brand" href="#"></a>
                                <p>Гарного настрою.</p>
                                <!-- Rights-->
                                <p class="rights"><span>©  </span><span class="copyright-year">2020</span><span> </span><span>Skype: </span><span>. </span><span>Delengov33.</span></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h5>Контакти</h5>
                            <dl class="contact-list">
                                <dt>Адресс:</dt>
                                <dd>м. Житомир вул Перовського буд 65 </dd>
                            </dl>
                            <dl class="contact-list">
                                <dt>Пошта:</dt>
                                <dd><a href="#">cnsgsrsy@gmail.com</a></dd>
                            </dl>
                            <dl class="contact-list">
                                <dt>Телефон:</dt>
                                <dd><a href="#">38066 674 8726 </a> <span>or</span> <a href="#">38095 580 7040</a>
                                </dd>
                            </dl>
                        </div>
                        <div class="col-md-4 col-xl-3">
                            <h5>Посилання</h5>
                            <ul class="nav-list">
                                <li><a href="/users/aboutus">Про нас</a></li>
                                <li><a href="#">Проект</a></li>
                                <li><a href="https://www.facebook.com/campaign/landing.php?campaign_id=1600506868&extra_1=s%7Cc%7C303839676210%7Ce%7Cfacebook%7C&placement=&creative=303839676210&keyword=facebook&partner_id=googlesem&extra_2=campaignid%3D1600506868%26adgroupid%3D60825658456%26matchtype%3De%26network%3Dg%26source%3Dnotmobile%26search_or_content%3Ds%26device%3Dc%26devicemodel%3D%26adposition%3D%26target%3D%26targetid%3Dkwd-541132862%26loc_physical_ms%3D1012846%26loc_interest_ms%3D%26feeditemid%3D%26param1%3D%26param2%3D&gclid=EAIaIQobChMIvYD2qp_m6QIVy5AYCh29vgMrEAAYASAAEgJkFfD_BwE">Соц мережа</a></li>
                                <li><a href="https://mail.google.com/mail/u/0/?hl=uk&view=cm&tf=1&fs=1&to=cnsgsrsy%40gmail.com">Контакти</a></li>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>


        </div>

</footer>












<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
<script src="/views/layout/js/main.js"></script>
</body>
</html>