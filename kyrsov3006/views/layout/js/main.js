window.onload = function (e) {
    var cart = {};// корзина
    var count_tov = 0;
    var title = {}//корзина назв
   // var costTov={};//вартість товару
    $('.add-to-cart').on('click', addTocart);

    /***
     * додаємо товар в корзину
     * **/
    function addTocart(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
         var sTitle= $(this).attr('data-title');
         var c_tov= $(this).attr('data-price');
        if (cart[id] == undefined) {
            cart[id] = 1;//якщо товару немає то він один
            count_tov++;
            title[id] = sTitle;
          //  costTov[id]=c_tov;

        } else {
            count_tov++;
            cart[id]++;// якщо товар вже є тоді збільшуємо його кількість

        }
        showMiniCart();
        saveCart();
    }

    //зберігаємо нашу корзину
    function saveCart() {
        localStorage.setItem('cart', JSON.stringify(cart));
        localStorage.setItem('count', JSON.stringify(count_tov));
        localStorage.setItem('dataTitle', JSON.stringify(title));

       // localStorage.setItem('dataPrice', JSON.stringify(costTov));
    }

    //перевіряю чи є в локалсторі  запись кар
    function loadCart() {
        if (localStorage.getItem('cart')) {
            cart = JSON.parse(localStorage.getItem('cart'));
            showMiniCart();
        }
        //раніше брали значення з html
      /*
        if (localStorage.getItem('dataPrice')) {
            costTov = JSON.parse(localStorage.getItem('dataPrice'));
            showMiniCart();
        }*/

        if (localStorage.getItem('dataTitle')) {
            title = JSON.parse(localStorage.getItem('dataTitle'));
            showMiniCart();
        }
//просто кількість товарів в таблиці
        if (localStorage.getItem('count')) {
            count_tov = JSON.parse(localStorage.getItem('count'));
            showMiniCart();
        }

    }

    //відображення міні корзини
    function showMiniCart() {
        // var out="";
        // for(var key in cart){
        //     out+=key+' --- '+cart[key]+'<br>';
        // }
        $('#mini-cart').html(count_tov);
    }

    loadCart();

}