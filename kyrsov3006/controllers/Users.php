<?php


namespace controllers;


class Users extends \core\Controller
{
    protected $Model;
   public function __construct()
   {
       $this->Model=new \models\Users();
   }

    public function actionIndex()
    {

    }
    /***
     *реєстація акаунта
     **/

    public function actionRegister(){
        session_start();
        if($this->isPost()){
            $post=$this->postFileter(['login','name','adress','password1','password2']);
            $post['password1'] =filter_var(trim( $post['password1']),FILTER_SANITIZE_STRING);
            $post['password2'] =filter_var(trim( $post['password2']),FILTER_SANITIZE_STRING);
            $post['name'] =filter_var(trim( $post['name']),FILTER_SANITIZE_STRING);
            $post['adress'] =filter_var(trim( $post['adress']),FILTER_SANITIZE_STRING);

            if(strcmp($post['password1'],$post['password2'])==0){
                $this->Model->Registrate($post['login'],$post['password1'],$post['adress'],$post['name']);
            }else{
                $_SESSION['regis_count']= " Паролі не збігаються";
            }


            /* echo $_POST['login'];
             echo $_POST['password'];
             echo"***";*/
            //var_dump($_POST);
        }
        return $this->render('globalfile');
    }
    /***
     *авторизація акаунта
     **/
    public function actionLogin(){
     //   echo $_SERVER['REQUEST_METHOD'];
        if($this->isPost()){
           $post=$this->postFileter(['login','password']);
// echo $post['login'];
 if($post['login']=='admin@user'){
     session_start();
     $_SESSION['acount_admin']='dkfdj!aslfkasjf';
 }
           $this->Model->Authenticate($post['login'],$post['password']);

           /* echo $_POST['login'];
            echo $_POST['password'];
            echo"***";*/
            //var_dump($_POST);
        }
        return $this->render('globalfile');

    }
    /****
     * Змінна параметрів акаунта(змінна персональних данних)
     */

    public function actionRebot(){
        //   echo $_SERVER['REQUEST_METHOD'];
        if($this->isPost()){
            $email= filter_var(trim($_POST['email']),FILTER_SANITIZE_STRING);
            $password=filter_var(trim($_POST['password']),FILTER_SANITIZE_STRING);
            $name=filter_var(trim($_POST['Sname']),FILTER_SANITIZE_STRING);

            $adress=filter_var(trim($_POST['adress']),FILTER_SANITIZE_STRING);
            $id=filter_var(trim($_POST['who']),FILTER_SANITIZE_STRING);
            session_start();
if($email==""||$password==""||$name==""||$adress==""){
    $_SESSION['regis_count2']="Коли ви останній раз хотіли змінити інформацію проіфлю ви ввели інформацію не коректно";
    return $this->render('profileuser');
}else{
    $_SESSION['regis_count2']='';
    $this->Model->RestarAcaunt($id,$email,$password,$name,$adress);
}


        }
        session_start();
        $_SESSION['login']=$email;//пошта
        $_SESSION['password'] =$password;//парль
        $_SESSION['lastname']=$name ;//імя
        $_SESSION['firstname']=$adress ;//dekbwz


        return $this->render('profileuser');

    }

    /****
     * інформаці про користувача
     *
     */
    public function actionProfile()
    {
        /*
         *
         *
         * */


       /* $_SESSION['login'] = $q['login'];
        $_SESSION['password'] = $q['password'];
        $_SESSION['lastname'] = $q['lastname'];//імя
        $_SESSION['firstname'] = $q['firstname'];//адрес доставкі*/


        return $this->render('profileuser');
    }

    /***
     *вивод всіх товарів
     **/
    public function actionAll(){
        $k=[];
        if($this->isGet()) {

            $show=filter_var(trim( $_GET['show']),FILTER_SANITIZE_STRING);
            $sort=filter_var(trim( $_GET['sort']),FILTER_SANITIZE_STRING);

           $k= $this->Model->ShowTovar($show,$sort);
            $k['pesi']=$show;
        }

       return $this->render('all',$k);
    }


    /****
     * карта продукта
     *
     */
    public function actionProduct(){
        $k=[];



            $k= $this->Model->ShowTovar();
           // $k['pesi']=$_GET['id'];
        $kk=$_GET['id']-1;
//echo $k[$kk]['id'];

        return $this->render('product',$k[$kk]);
    }



    /*****
     * Корзина
     *
     */
    public function actionBasket(){
               $k=[];
      /*  if (localStorage.getItem('dataPrice')) {
            $k = JSON.parse(localStorage.getItem('dataPrice'));
           // showMiniCart();
        }else{
               */
        $k= $this->Model->ShowPrice();
      /*  }
        localStorage.setItem('dataPrice', JSON.stringify($k));*/


        return $this->render('basket',$k);
    }
    /***
     *вихід з акаунта
     **/
    public function actionExit(){

       $this->Model->somExit();
        ////users/globalfile
        return $this->render('globalfile');
    }
    /****
     * сторінко про нас
     *
     */
    public function actionAboutus(){
        return $this->render('aboutus');

    }
    /****
     * Сторінка головна
     *
     */

    public function actionGlobalfile(){
        return $this->render('globalfile');

    }



}